# Dockerfile
FROM registry.gitlab.com/yade-dev/docker-yade:ubuntu20.04

ENV DEBIAN_FRONTEND=noninteractive

RUN bash -c 'echo "deb http://www.yade-dem.org/packages/ focal main" >> /etc/apt/sources.list' && \
    wget -O - http://www.yade-dem.org/packages/yadedev_pub.gpg | sudo apt-key add - && \
    apt-get update && \
    apt-get -y  install yadedaily yadedaily-doc


